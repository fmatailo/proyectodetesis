<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migration_Tabla_pension extends CI_Migration {

        /**
         * CREATE TABLE pension IN DATABASE
         * @param null
         */
        public function up(){
            $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'fecha_pago' => array('type' => 'DATE'),
                'fecha_pagado' => array('type' => 'DATE'),
                'monto' => array('type' => 'FLOAT', 'constraint' => 11),
                'estado' => array('type' => 'ENUM("0","1")'),
                'slug' => array('type' => 'VARCHAR', 'constraint' => 80, 'unique'=> TRUE),
                'matricula_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            ));
            $this->dbforge->add_key('id', TRUE);
            $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (matricula_id) REFERENCES matricula(id)");
            $this->dbforge->create_table('pension', TRUE, ['ENGINE' => 'InnoDB']);
        }

        /**
         * DROP DATABASE pension IN DATABASE
         * @param null
         */
        public function down(){
            $this->dbforge->drop_table('pension');
        }
    }