<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    // Incluimos el archivo fpdf
    require_once APPPATH."/third_party/fpdf/fpdf.php";

    //Extendemos la clase Pdf de la clase fpdf para que herede todas sus variables y funciones
    class Pdf extends FPDF {

        var $widths;
        var $aligns;
        var $title;

        public function __construct() {
            parent::__construct();
        }

        // El encabezado del PDF
        public function Header(){
            $this->Image(base_url().'assets/images/logo.jpg',15,12,15);
            $this->Ln(6);
            $this->SetFont('Arial','B',16);
            $this->Cell(0,10,'ACADEMIA DE NATACION',0,1,'C');
            $this->SetFont('Arial','B',8);
            $this->Cell(0,2,utf8_decode($this->title),0,0,'C');
            $this->Ln(10);
        }
        // El pie del pdf
        public function Footer(){
            $y = -40;
            $this->SetY($y);
            $this->Cell(0,10,'______________________',0,0,'C');
            $this->SetY($y+5);
            $this->Cell(0,10,utf8_decode('FIRMA'),0,0,'C');
            $this->SetY(-21);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,utf8_decode('Pagina '.$this->PageNo().'/{nb}'),0,0,'C');
        }

        function SetWidths($w){
            //Set the array of column widths
            $this->widths=$w;
        }

        function SetTitleHead($t){
            $this->title=$t;
        }

        function SetAligns($a){
            //Set the array of column alignments
            $this->aligns=$a;
        }

        function head_table($data, $align){
            $this->SetFont('Arial','B',10);
            for($i=0;$i<count($data);$i++)
                $this->Cell($align[$i],8,utf8_decode($data[$i]),1,0,'C');
            $this->Ln();
        }

        function table($inf){
            $this->SetFont('Arial','',10);
            foreach($inf as $data){
                //Calculate the height of the row
                $nb=0;
                for($i=0;$i<count($data);$i++)
                    $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
                $h=8*$nb;
                //Issue a page break first if needed
                $this->CheckPageBreak($h);
                //Draw the cells of the row
                for($i=0;$i<count($data);$i++)
                {
                    $w=$this->widths[$i];
                    $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
                    //Save the current position
                    $x=$this->GetX();
                    $y=$this->GetY();
                    //Draw the border
                    $this->Rect($x,$y,$w,$h);
                    //Print the text
                    $this->MultiCell($w,8,utf8_decode($data[$i]),0,$a);
                    //Put the position to the right of the cell
                    $this->SetXY($x+$w,$y);
                }
                //Go to the next line
                $this->Ln($h);
            }
        }

        function CheckPageBreak($h){
            //If the height h would cause an overflow, add a new page immediately
            if($this->GetY()+$h>$this->PageBreakTrigger)
                $this->AddPage($this->CurOrientation);
        }

        function NbLines($w,$txt){
            //Computes the number of lines a MultiCell of width w will take
            $cw=&$this->CurrentFont['cw'];
            if($w==0)
                $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r",'',$txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
            while($i<$nb)
            {
                $c=$s[$i];
                if($c=="\n")
                {
                    $i++;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                    continue;
                }
                if($c==' ')
                    $sep=$i;
                $l+=$cw[$c];
                if($l>$wmax)
                {
                    if($sep==-1)
                    {
                        if($i==$j)
                            $i++;
                    }
                    else
                        $i=$sep+1;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                }
                else
                    $i++;
            }
            return $nl;
        }

        function tabla_color($inf){
            // Cabeceras
            $this->SetFont('Arial','B',10);
            for($i=0;$i<count($inf['cabecera']);$i++)
                $this->Cell($inf['tamaño'][$i],8,utf8_decode($inf['cabecera'][$i]),1,0,'C');
            $this->Ln();
            // Datos
            $this->SetFillColor(224,235,255);
            $this->SetTextColor(0);
            $this->SetFont('Arial','',10);
            // Datos
            $fill = FALSE;
            foreach($inf['contenido'] as $row)
            {
                for($i=0;$i<count($inf['tamaño']);$i++)
                    $this->Cell($inf['tamaño'][$i],8,utf8_decode($row),'LR',0,'L', $fill);
                $this->Ln();
                $fill = !$fill;
            }
            // Línea de cierre
            $this->Cell(array_sum($inf['tamaño']),0,'','T');
        }
    }
?>