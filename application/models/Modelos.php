<?php
class Modelos extends CI_Model { 

   public function __construct() {
      parent::__construct();
   }

   /**
     * @method
     * @param
     */
    public function perfil(array $data){
        $level=null;
        switch($data['tipo']){
            case 'administrador':
                $level=1;
                break;
            case 'usuario':
                $level = 2;
                break;
            case 'coreografo':
                $level = 3;
                break;
            case 'cliente':
                $level = 4;
                break;
        }
        $perfil = new Perfiles;
        $perfil->cedula = $data['cedula'];
        $perfil->apellidos = $data['apellidos'];
        $perfil->nombres = $data['nombres'];
        $perfil->direccion = $data['direccion'];
        $perfil->email = $data['email'];
        $perfil->genero = $data['genero'];
        $perfil->telefono = $data['telefono'];
        $perfil->edad = $data['edad'];
        $perfil->estado = $data['estado'];
        $perfil->username = $data['username'];
        $perfil->password = password_hash($data['password'], PASSWORD_DEFAULT);
        $perfil->tipo = $data['tipo'];
        $perfil->informacion = $data['informacion'];
        $perfil->level_to = $level;
        $perfil->img_perfil = $data['img_perfil'];
        $perfil->slug = substr(md5(mt_rand()), 0, 40).$data['cedula'];
        $perfil->save();
    }

    /**
     * @method
     * @param
     */
    public function curso(array $data){
        $curso = new Cursos;
        $curso->disciplina = $data['disciplina'];
        $curso->descripcion = $data['descripcion'];
        $curso->img_curso = $data['img_curso'];
        $curso->estado = $data['estado'];
        $curso->slug = substr(md5(mt_rand()), 0, 40).$data['disciplina'];
        $curso->save();
    }

    /**
     * @method
     * @param
     */
    public function horario(array $data){
        $tipo = null;
        $duracion = null;
        switch ($data['tipo_curso']){
            case 1:
                $tipo = 'permanente';
                $duracion = '10';
                break;
            case 2:
                $tipo = 'temporal';
                $duracion = '4';
                break;
            case 3:
                $tipo = 'vacacional';
                $duracion = '2';
                break;
        }
        $horario = new Horarios;
        $horario->$tipo;
        $horario->fecha_inicio = $data['fecha_inicio'];
        $horario->costo = $data['costo'];
        $horario->cupo = $data['cupo'];
        $horario->fecha_fin = date("Y-m-d", strtotime(date('Y-m-d', strtotime($data['fecha_inicio']))." + ".$duracion." months"));
        $horario->duracion = $duracion;
        $horario->hora_inicio = $data['hora_inicio'];
        $horario->hora_fin = $data['hora_fin'];
        $horario->estado = $data['estado'];
        $horario->slug = substr(md5(mt_rand()), 0, 40);
        $horario->curso_id = $data['curso_id'];
        $horario->coreografo_id = $data['coreografo_id'];
        $horario->save();
    }

    public function inscripcion(array $data){
        $inscripcion = new Inscripciones;
        $inscripcion->fecha_inscripcion = $data['fecha_inscripcion'];
        $inscripcion->estado = $data['estado'];
        $inscripcion->horario_id = $data['horario_id'];
        $inscripcion->cliente_id = $data['cliente_id'];
        $inscripcion->slug = substr(md5(mt_rand()), 0, 40);
        $inscripcion->save();
    }

    public function matricula(array $data){
        $matricula = new Matriculas;
        $matricula->fecha_matricula = $data['fecha_matricula'];
        $matricula->estado = $data['estado'];
        $matricula->inscripcion_id = $data['inscripcion_id'];
        $matricula->slug = substr(md5(mt_rand()), 0, 40);
        $matricula->save();
        return $matricula;
    }

    public function pension(array $data){
        $matricula = new Pensiones;
        $matricula->fecha_pago = $data['fecha_pago'];
        $matricula->monto = $data['monto'];
        $matricula->estado = $data['estado'];
        $matricula->matricula_id = $data['matricula_id'];
        $matricula->slug = substr(md5(mt_rand()), 0, 40);
        $matricula->save();
    }

    public function contenido(array $data){
        $contenido = new Contenidos;
        $contenido->titulo = $data['titulo'];
        $contenido->descripcion = $data['descripcion'];
        $contenido->img = $data['img'];
        $contenido->save();
    }
}