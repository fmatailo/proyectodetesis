<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Matriculas extends Eloquent {

    protected $table = "matricula"; // table name

    /**
     * @method
     * @param
     */
    public function inscripcion(){
        return Inscripciones::find($this->inscripcion_id);
    }

    /**
     * @method
     * @param
     */
    public function pensiones(){
        return Pensiones::all()->where('matricula_id',$this->id);
    }

    /**
     * @method
     * @param
     */
    public function pensiones_estado($state=1){
        $state = (string)$state;
        if (Pensiones::where('matricula_id',$this->id)->where('estado', $state)->exists()){
            return TRUE;
        }
        return FALSE;
    }
}

# Execute composer dump-autoload