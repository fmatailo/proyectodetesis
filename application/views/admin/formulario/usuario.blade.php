@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id' => "demo-form2",
                        'data-parsley-validate' => "",
                        'class' =>"form-horizontal form-label-left"
                    );
                    echo form_open_multipart('', $attr); 
                    ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Cédula<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" min="0" id="last-name" name="cedula" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('cedula'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Apellidos<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" name="apellidos" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('apellidos'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nombres<span class="required">:</span></label>  
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="first-name" required="required" name="nombres" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('nombres'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Dirección<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" name="direccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('direccion'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Email<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" id="last-name" name="email" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('email'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Genero</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label for="genderM">Masculino:</label>
                                <input type="radio" class="flat" name="genero" id="genderM" value="1" required /> 
                                <label for="genderF">Femenino:</label>  
                                <input type="radio" class="flat" name="genero" id="genderF" value="0" required/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Teléfono<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" min="0" id="last-name" name="telefono" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('telefono'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Edad<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" min="0" id="last-name" name="edad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('edad'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Imagen de Perfil</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" id="last-name" name="img_perfil" class="form-control col-md-7 col-xs-12" accept="image/*">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Usuario<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="last-name" name="username" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('username'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Contraseña<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="last-name" name="password" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Repite la contraseña<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="last-name" name="password2" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="reset" value="Limpiar" class="btn btn-primary">
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection