@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id'                    => "demo-form2",
                        'data-parsley-validate' => "",
                        'class'                 =>"form-horizontal form-label-left"
                    );
                    echo form_open('', $attr); 
                    ?>  
    
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cédula</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="discipina" class="form-control col-md-7 col-xs-12" value='{{ $cedula }}' readonly>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cliente</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="cliente" class="form-control col-md-7 col-xs-12" value='{{ $cliente }}' readonly>
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Curso</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="curso" class="form-control col-md-7 col-xs-12" value='{{ $curso }}' readonly>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Horario
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="horario" class="form-control col-md-7 col-xs-12" value='{{ $horario }}' readonly>
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Costo</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="costo" class="form-control col-md-7 col-xs-12" value='{{ $costo }}' readonly>
                            </div>
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Fecha de Matricula</label>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <input type="text" id="fecha_matricula" class="form-control col-md-7 col-xs-12" value='{{ $fecha_matricula }}' readonly>
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="curso">Forma de Pago</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="pago" id="pago" required>
                                    <option value="">----------</option>
                                    <option value='1'>Contado</option>
                                    <option value='2'>Cuotas</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" id="tabla_matricula">
                        <!-- Ajax -->
                        </div>
                        </div>
            
            
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function(){
            $("#pago").change(function () {
                if ($('#pago').val() != 2){
                    $('#tabla_matricula').html('')
                }else{
                    $.ajax({
                        data: {'slug' : "<?= $slug ?>"},
                        url: "<?= base_url() ?>ajax/template_matricula",
                        type: 'post',
                        dataType: 'html',
                        success: function(data){ 
                            $('#tabla_matricula').html(data);
                        }
                    })
                }
            })
        })
    </script>
@endsection