@extends('template/base_admin')

@section('style')
<!-- bootstrap-daterangepicker -->
<link href="<?= base_url() ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
@endsection

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id'                    => "demo-form2",
                        'data-parsley-validate' => "",
                        'class'                 =>"form-horizontal form-label-left"
                    );
                    echo form_open('', $attr); 
                    ?>  
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="curso">Curso </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="curso" class="form-control" name="curso" required>
                                    <option value="">----------</option>
                                    @foreach ($cursos as $row)
                                    <option value={{ $row->id }}>{{ $row->disciplina }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="coreografo">Instructor </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="coreografo" class="form-control" name="coreografo" required>
                                    <option value="">----------</option>
                                    @foreach ($perfiles as $row)
                                    <option value={{ $row->id }}>{{ $row->nombres.' '.$row->apellidos }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo">Tipo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="tipo" class="form-control" name="tipo" required>
                                    <option value="">----------</option>
                                    <option value="1">Permanente</option>
                                    <option value="2">Temporal</option>
                                    <option value="3">Vacacional</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cupo">Cupos Disponibles<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="cupo" name="cupo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('cupo'); ?>">
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="costo">Costo<span class="required">:</span></label>  
                            <div class="col-md-6 col-sm-6 col-xs-12" >
                                <input type="number" step="0.01" id="costo" required="required" name="costo" class="form-control col-md-7 col-xs-12 has-feedback-left" placeholder="0.00" value="<?php echo set_value('nombres'); ?>">
                                <span class="fa fa-usd form-control-feedback left" aria-hidden="true"></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fecha_inicio">Fecha Inicio<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" required="required" class="form-control has-feedback-left fecha" name="fecha_inicio" id="fecha_inicio" aria-describedby="inputSuccess2Status3">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                <span id="inputSuccess2Status2" class="sr-only">(success)</span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="inicio">Hora de inicio<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="time" id="inicio" name="inicio" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('inicio'); ?>">
                            </div>
                        </div>
            
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fin">Hora de culminación<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="time" id="fin" name="fin" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo set_value('fin'); ?>">
                            </div>
                        </div>
            
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="reset" value="Limpiar" class="btn btn-primary">
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- bootstrap-daterangepicker -->
<script src="<?= base_url() ?>assets/vendors/moment/min/moment.min.js"></script>
<script src="<?= base_url() ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(function(){
        $('.fecha').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD-MM-YYYY'
            },
        })

    });
</script>
@endsection