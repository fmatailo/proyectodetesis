@extends('template/base_admin')

@section('style')
    <!-- Select2 -->
    <link href="<?= base_url() ?>assets/vendors/Select2/css/select2.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>

                    <?php 
                    $attr = array(
                        'id' => "demo-form2",
                        'data-parsley-validate' => "",
                        'class' =>"form-horizontal form-label-left"
                    );
                    echo form_open('', $attr); 
                    ?>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Cédula<span class="required">:</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="pension" class="form-control search_select" name="pension" required>
                                    <option value="">----------</option>
                                    @foreach (Perfiles::all()->where('level_to', '4') as $row)
                                    <option value={{ $row->id }}>{{ $row->get_full_name() }} - {{ $row->cedula }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="/admin/inicio" class="btn btn-primary">Cancelar</a>
                                <input type="submit" value="Enviar" class="btn btn-success">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <!-- Select2 -->
    <script src="<?= base_url() ?>assets/vendors/Select2/js/select2.full.min.js"></script>
    <script>
        $('.search_select').select2();
    </script>
@endsection