@extends('template/base_admin')

@section('content')
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2> <strong>{{ $title }}</strong></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Num.</th>
                                <th>Cliente</th>
                                <th>Cédula</th>
                                <th>Curso</th>
                                <th>Instructor</th>
                                <th>Duración</th>
                                <th>Costo</th>
                                <th style="width:110px">Fecha Inscripcion</th>
                                <th >Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=0 ?>
                            @foreach ($inscripciones as $row)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $row->cliente()->nombres.' '.$row->cliente()->apellidos }}</td>
                                <td>{{ $row->cliente()->cedula }}</td>
                                <td>{{ $row->horario()->curso()->disciplina }}</td>
                                <td>{{ $row->horario()->coreografo()->nombres.' '.$row->horario()->coreografo()->apellidos }}</td>
                                <td>{{ $row->horario()->duracion }} meses</td>
                                <td>{{ money_format('%.2n', $row->horario()->costo) }}</td>
                                <td>{{ $row->fecha_inscripcion }}</td>
                                <td>
                                    <a href="/admin/matricula/crear/{{ $row->slug }}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a>
                                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target=".bd-example-modal-sm"><i class="fa fa-close"></i></button>
                                </td> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <!-- Datatables -->
    <script src="<?= base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url(); ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
@endsection