@extends('template/base')

@section('content')

<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                <div id="system-message-container"></div>

                <?php
                $attr = array(
                    'class' => 'contacto uk-text-center'
                )
                ?>
                <?php echo form_open('', $attr); ?>
                <?php echo validation_errors(); ?>
                    <div>
                        <label><strong style="font-size: 16px">FECHA</strong> </label>
                    <input type='text' value="{{ $fecha }}" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">CÉDULA </strong></label>
                        <input type='text' value="{{ $cedula }}" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">APELLIDOS </strong></label>
                        <input type='text' value="{{ $apellidos }}" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">NOMBRES </strong></label>
                        <input type='text' value="{{ $nombres }}" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">DIRECCIÓN </strong></label>
                        <textarea name="direccion" id="" rows="3" readonly>{{ $direccion }}</textarea>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">TELÉFONO </strong></label>
                        <input type='text' value="{{ $telefono }}" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">CURSO </strong></label>
                        <select name="curso" id="curso">
                            <option value>-----------</option>
                            @foreach (Cursos::all()->where('estado',1) as $row)
                            <option value={{ $row->id }}>{{ $row->disciplina }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">FECHA DE INICIO </strong></label>
                        <select name="fecha_inicio" id="fecha_inicio">
                            <option value>-----------</option>
                        </select>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">HORARIO </strong></label>
                        <input type='text' id="horario" name="horario" readonly>
                    </div>
                    <div>
                        <label><strong style="font-size: 16px">CUPOS DISPONIBLES </strong></label>
                        <input type='text' id="cupos" name="cupos" readonly>
                    </div>
                    <br>
                    <input type="submit" id="aiContactSafeSendButton_2" style="width: 225px;" value="Enviar">
                
                </form>
            </main>
        </div>
    </div>
</div>

@endsection

@section('script')
    <!-- Curso - Horario -->
    <script>
        $(document).ready(function(){
            var horario;
            $("#curso").change(function () {
                $('#fecha_inicio').empty();
                $('#fecha_inicio').append('<option value="">-----------</option>');
                $('#horario').val('');
                $('#cupos').val('');
                $.ajax({
                    url: "<?= base_url() ?>ajax/horario",
                    type: 'post',
                    dataType: 'json',
                    success: function(data){ 
                        horario = data;
                        data.forEach(element => {
                            if (element['curso_id'] == $('#curso').val()){
                                var fecha = new Date(element['fecha_inicio']);
                                var options = { year: 'numeric', month: 'long', day: 'numeric' };
                                fecha = fecha.toLocaleDateString("es-ES", options);
                                $('#fecha_inicio').append('<option value='+element['id']+'>'+fecha+'</option>');
                            }
                        });
                    }
                })
            })
            $("#fecha_inicio").change(function () {
                horario.forEach(element => {
                    if (element['id'] == $('#fecha_inicio').val()){
                        $('#horario').val(element['hora_inicio']+' - '+element['hora_fin']);
                        $('#cupos').val(element['cupo']);
                        return false;
                    }
                });
            })
        });
        
    
    </script>
@endsection