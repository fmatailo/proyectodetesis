@extends('template/base')

@section('content')
<div class="tm-fullscreen uk-position-relative">
    
    <div class="akslider-module ">
        <div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'scale', duration: '500', autoplay: false, autoplayInterval: '7000', videoautoplay: false, videomute: true, kenburns: true}">
            <ul class="uk-slideshow">
                <li class="uk-cover uk-height-viewport uk-active">
                    <div class="uk-cover-background uk-position-cover uk-animation-scale uk-animation-reverse uk-animation-15 uk-animation-middle-left" style="background-image: url(images/demo/visuals/slide-contacts.jpg);"></div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d995.0222762223872!2d-79.19654313421695!3d-4.002103102392491!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91cb38017a9278a9%3A0x74417ca485d81769!2sLourdes+%26+Sabiango%2C+Loja!5e0!3m2!1ses!2sec!4v1541379309891" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    
                </li>
            </ul>
        </div>
    </div>
    
</div>

<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                
                <div id="system-message-container"></div>
                
                <!-- START Article block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">
                        Localización
                    </h1>
                    <div>
                        <div class="uk-grid">
                            <div class="uk-width-3-5">
                                <p style="text-align: justify">La Academia de natación aQuaro Ecole se encuentra ubicada en las calles Sabiando #13-50 y Lourdes la cual estará gusto de atendeler en sus instalaciones para brindar toda la infromación que usted requiera hacer de los cursos que ofrecen a la cuidania en general.</p>
                                <p style="text-align: justify">Somos una Academia de natación que comienza su recorrido en Septiembre de 1973 con la finalidad de brindarle a la comunidad Lojana un espacio para desarrollar y culminar sus disciplinas en el campo laboral.</p>
                                <div class="uk-grid">
                                    <div class="uk-width-1-3">
                                        <h3 class="uk-margin-top">Dr. Antonio Quizhpe</h3>
                                        <p>calles: Sabiando #13-50<br>
                                            y Lourdes <br>
                                    Telefonos:(02)575740 / 0993858170
                                        </p>
                                    </div>
                                    <div class="uk-width-1-3">
                                        
                                    </div>
                                    <div class="uk-width-1-3">
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-2-5">
                                <h3>Contactanos!</h3>
                                <div>
                                    <div id="aiContactSafe_form_2">
                                        <div class="aiContactSafe" id="aiContactSafe_mainbody_2">
                                            <div class="contentpaneopen">
                                                <div id="aiContactSafeForm_2">
                                                    <form action="#" method="post" id="adminForm_2" name="adminForm_2" enctype="multipart/form-data">
                                                        <div id="displayAiContactSafeForm_2">
                                                            <div class="aiContactSafe" id="aiContactSafe_contact_form_2">
                                                                <div class="aiContactSafe_row">    
                                                                    <div class="aiContactSafe_contact_form_field_right"><input type="text" name="aics_name_ph" id="aics_name_ph" class="textbox" placeholder="Nombre" value=""></div>
                                                                </div>
                                                                <div class="aiContactSafe_row">
                                                                    <div class="aiContactSafe_contact_form_field_right"><input type="text" name="aics_email_ph" id="aics_email_ph" class="email" placeholder="Correo" value=""></div>
                                                                </div>
                                                                <div class="aiContactSafe_row">
                                                                    <div class="aiContactSafe_contact_form_field_right"><textarea name="aics_message" id="aics_message" cols="40" rows="10" class="editbox" placeholder="Mensaje"></textarea></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div id="aiContactSafeBtns_2">
                                                            <div id="aiContactSafeButtons_left" style="clear:both; display:block; width:100%; text-align:left;">
                                                                <div id="aiContactSafeSend_2" style="float:left;">
                                                                    <div id="aiContactSafeSend_loading_2" style="float:left; margin:2px;">&nbsp;</div>
                                                                    <input type="submit" id="aiContactSafeSendButton_2" value="Enviar" style="float:left; margin:2px;">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- END Article block -->
            </main>
        </div>
    </div>
</div>
@endsection