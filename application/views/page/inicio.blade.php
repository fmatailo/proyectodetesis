<!DOCTYPE HTML>
<html lang="en-gb" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="With unique design and accent in details Maxx Fitness is perfect template. Design have beautiful typography and elegant structure. HTML Template is based on Warp7 Framework and made for all who wants a lightweight and modular website.">
        
        <title>Academia aQuaro-Ecole</title>
        
        <!-- Favicon and Apple icon -->
        <link href="<?= base_url() ?>assets/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <link href="apple_touch_icon.png" rel="apple-touch-icon-precomposed">
        
        <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet"><!-- Bootstrap Styles -->
        <link href="<?= base_url() ?>assets/css/theme.css" rel="stylesheet"><!-- Main Template Styles --> 
        <link href="<?= base_url() ?>assets/css/schedule.css" rel="stylesheet"><!-- Schedule CSS -->
    </head>
    <body class="tm-isblog">
        <div class="ak-page">
            
            <!-- START Main menu -->
            <nav class="tm-navbar uk-navbar">
                <div class="uk-container uk-container-center">
                    <!-- START Logo -->
                    <a class="tm-logo" href="index.html">
                        <span class="color-1">ACADEMIA</span><span class="color-2"> DE NATACIÓN</span>
                    </a>
                    <!-- END Logo -->
                    <ul class="uk-navbar-nav uk-hidden-small">
                        @include('include/nav')
                    </ul>
                    <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas=""></a>
                </div>
            </nav>
            <!-- END Main menu -->
            
            <!-- START Fullscreen block -->
            <div class="tm-fullscreen uk-position-relative">
                <div class="akslider-module ">
                    <div class="uk-slidenav-position" data-uk-slideshow="{height: 'auto', animation: 'scale', duration: '500', autoplay: false, autoplayInterval: '7000', videoautoplay: true, videomute: true, kenburns: false}">
                        <ul class="uk-slideshow">
                            <li class="uk-cover uk-height-viewport uk-active">
                                <!-- START Slide 1 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_1.jpg');"></div>
                                <img src="<?= base_url() ?>assets/images/demo/slider/slide_1.jpg" width="800" height="400" alt="Go to Gym">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle">
                                    <div class="tm-slide-style-1">
                                        <h3 class="slide-head tm-heading-font">
                                            
                                        </h3>
                                        <div class="slide-text-primary tm-heading-font">
                                            <a href="#"></a>
                                        </div>
                                        <h4 class="slide-subhead uk-text-right tm-heading-font">
                                            <div id="aiContactSafe_form_1" class="uk-text-center">
                                                <a href="/page/sing_up" id="aiContactSafeSendButton" style="margin:5%;"><strong>INSCRÍBETE</strong></a>
                                            </div>
                                        </h4>
                                    </div>
                                </div>
                                <!-- END Slide 1 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 2 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(<?= base_url() ?>assets/images/demo/slider/slide_2.jpg);"></div>
                                <img src="<?= base_url() ?>assets/images/demo/slider/slide_2.jpg" width="800" height="400" alt="Kickboxing">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle"></div>
                                <!-- END Slide 2 -->
                            </li>
                            
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 3 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(<?= base_url() ?>assets/images/demo/slider/slide_3.jpg);"></div>
                                <img src="<?= base_url() ?>assets/images/demo/slider/slide_3.jpg" width="800" height="400" alt="Muscle Mass">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle"></div>
                                <!-- END Slide 3 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 4 -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(<?= base_url() ?>assets/images/demo/slider/slide_4.jpg);"></div>
                                <img src="<?= base_url() ?>assets/images/demo/slider/slide_4.jpg" width="800" height="400" alt="Go to Gym">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle"></div>
                                <!-- END Slide 4 -->
                            </li>
                            <li class="uk-cover uk-height-viewport">
                                <!-- START Slide 5 (Video) -->
                                <div class="uk-cover-background uk-position-cover" style="background-image: url(<?= base_url() ?>assets/images/demo/slider/slide_5.jpeg);"></div>
                                <img src="<?= base_url() ?>assets/images/demo/slider/slide_5.jpeg" width="800" height="400" alt="Go to Gym">
                                <div class="uk-caption uk-caption-panel uk-animation-fade uk-flex uk-flex-center uk-flex-middle"></div>
                                <!-- END Slide 5 (Video) -->
                            </li>
                        </ul>
                        
                        <!-- START Slider next/prev buttons -->
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                        <!-- END Slider next/prev buttons -->
                        
                        <!-- START Slider navigator -->
                        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-text-center">
                            <li data-uk-slideshow-item="0" class="uk-active"><a href="#" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_1_btn.jpg')">0</a></li>
                            <li data-uk-slideshow-item="1"><a href="#" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_2_btn.jpg')">1</a></li>
                            <li data-uk-slideshow-item="2"><a href="#" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_3_btn.jpg')">2</a></li>
                            <li data-uk-slideshow-item="3"><a href="#" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_4_btn.jpg')">3</a></li>
                            <li data-uk-slideshow-item="4"><a href="#" style="background-image: url('<?= base_url() ?>assets/images/demo/slider/slide_5_btn.jpeg')">4</a></li>
                        </ul>
                        <!-- END Slider navigator -->

                    </div>
                </div>
            </div>
            <!-- END Fullscreen block -->
            
            <!-- START Features block -->
            <div class="tm-block  tm-block-top-a tm-section-light tm-section-padding-large">
                <div class="uk-container uk-container-center">
                    <section class="tm-top-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1">
                            <div class="uk-panel">
                                <div class="category-module-features ">
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                        <div class="uk-width-medium-1-3 uk-width-large-1-3 uk-text-center">
                                            <!-- START Feature 1 -->
                                            <div class="uk-overlay tm-overlay uk-width-1-1">
                                                <div class="uk-panel uk-panel-box">
                                                    <img src="<?= base_url() ?>assets/images/demo/visuals/ico-classes.png" alt="Disciplinas">
                                                    <h2 class="uk-panel-title uk-margin-top">Cursos</h2>
                                                    <p class="tm-panel-subtitle">La academia de natación aQuaro Ecole posee diferentes disciplians para que los interesados en aprender en nadar y practicar este deporte.</p>
                                                </div>
                                                <div class="uk-overlay-area">
                                                    <div class="uk-overlay-area-content">
                                                        <p>La academia de nataión aQuaro Ecole posee diferentes disciplians para que los interesados en nadar y practicar este deporte .</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="uk-button uk-button-large" href="curso" title="View Workouts">
                                                <i class="uk-icon-akplus">+</i>
                                                <span>Visitar</span>
                                            </a>
                                            <!-- END Feature 1 -->
                                        </div>
                                        <div class="uk-width-medium-1-3 uk-width-large-1-3 uk-text-center">
                                            <!-- START Feature 2 -->
                                            <div class="uk-overlay tm-overlay uk-width-1-1">
                                                <div class="uk-panel uk-panel-box">
                                                    <img src="<?= base_url() ?>assets/images/demo/visuals/ico-schedule.png" alt="Horarios">
                                                    <h2 class="uk-panel-title uk-margin-top">Horarios</h2>
                                                    <p class="tm-panel-subtitle">La academia de natación aQuaro Ecole posee diferentes horarios en cada una de sus disciplinas para que los usuarios escogan.</p>
                                                </div>
                                                <div class="uk-overlay-area">
                                                    <div class="uk-overlay-area-content">
                                                        <p>La academia de natación aQuaro Ecole posee diferentes diferentes horarios en cada una de sus disciplinas para que los usuarios escogan.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="uk-button uk-button-large" href="horario" title="View Classes">
                                                <i class="uk-icon-akplus">+</i>
                                                <span>Visitar</span>
                                            </a>
                                            <!-- END Feature 2 -->
                                        </div>
                                        <div class="uk-width-medium-1-3 uk-width-large-1-3 uk-text-center">
                                            <!-- START Feature 3 -->
                                            <div class="uk-overlay tm-overlay uk-width-1-1">
                                                <div class="uk-panel uk-panel-box">
                                                    <img src="<?= base_url() ?>assets/images/demo/visuals/ico-trainers.png" alt="COREÓGRAFOS">
                                                    <h2 class="uk-panel-title uk-margin-top">INSTRUCTORES</h2>
                                                    <p class="tm-panel-subtitle">La academia de natación aQuaro Ecole cuenta con instructores profesionales para la enseñanza de los diferentes disciplinas que posee la academia.</p>
                                                </div>
                                                <div class="uk-overlay-area">
                                                    <div class="uk-overlay-area-content">
                                                        <p>La academia de natación aQuaro Ecole cuenta con instructores profesionales para la enseñanza de los diferentes disciplinas que posee.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="uk-button uk-button-large" href="coreografo" title="View Schedule">
                                                <i class="uk-icon-akplus">+</i>
                                                <span>Visitar</span>
                                            </a>
                                            <!-- END Feature 3 -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- END Features block -->
            
            <!-- START About block -->
            @php $contenido=Contenidos::all()->last() @endphp
            @if ($contenido)
            <div class="tm-block  tm-block-top-b tm-section-dark tm-section-padding-large">
                <div class="uk-container uk-container-center">
                    <section class="tm-top-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1">
                            <div class="uk-panel">
                                <div class="uk-grid" style="margin-bottom: 110px;">
                                    <div class="uk-width-large-1-2 uk-width-medium-2-3 uk-width-small-1-1">
                                        <h4 class="tm-logo-text">
                                            <span class="color-2">{{ $contenido->titulo }}</span>
                                        </h4>
                                        <p class="uk-margin-remove">{{ $contenido->descripcion }}</p>
                                        <hr style="margin: 35px 0;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- END About block -->
            
            @if ($contenido->img)
            <!-- START Statistic and Promo block -->
            <div class="tm-block  tm-block-top-c tm-section-light">
                <div class="uk-container uk-container-center">
                    <section class="tm-top-c uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1 uk-width-medium-1-2">
                            <div class="uk-panel">
                                <!-- START Statistic block -->
                                <!-- END Statistic block -->
                            </div>
                        </div>
                        <div class="uk-hidden-small uk-width-medium-1-2">
                            <!-- START Promo block -->
                            <div class="uk-panel uk-hidden-small">
                                <a class="tm-promo-video" href="https://vimeo.com/31012462" data-uk-lightbox="{group:'group2'}">
                                    @if ($contenido->img)
                                    <img src="<?= base_url() ?>uploads/{{ $contenido->img }}" width="610" height="380" alt="Promo Video">     
                                    @else
                                    <img src="<?= base_url() ?>uploads/images/demo/visuals/promo-video.jpg" width="610" height="380" alt="Promo Video"> 
                                    @endif
                                </a>
                            </div>
                            <!-- END Promo block -->
                        </div>
                    </section>
                </div>
            </div>
            <!-- END Statistic and Promo block -->
            @endif
            @else
                <!-- START About block -->
                <div class="tm-block  tm-block-top-b tm-section-dark tm-section-padding-large">
                    <div class="uk-container uk-container-center">
                        <section class="tm-top-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                            <div class="uk-width-1-1">
                                <div class="uk-panel">
                                    <div class="uk-grid" style="margin-bottom: 110px;">
                                        <div class="uk-width-large-1-2 uk-width-medium-2-3 uk-width-small-1-1">
                                            <h4 class="tm-logo-text">
                                                <span class="color-1">ACADEMIA</span><span class="color-2"> DE NATACIÓN</span>
                                            </h4>
                                            <p class="uk-margin-remove">We are Spotys. Premium HTML Template for small and big gyms. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper.</p>
                                            
                                            <hr style="margin: 35px 0;">
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <!-- END About block -->

                <!-- START Statistic and Promo block -->
                <div class="tm-block  tm-block-top-c tm-section-light">
                    <div class="uk-container uk-container-center">
                        <section class="tm-top-c uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                            <div class="uk-width-1-1 uk-width-medium-1-2">
                                <div class="uk-panel">
                                    <!-- START Statistic block -->
                                    <div class="uk-panel uk-text-center ak-statistics-box">
                                        
                                    </div>
                                    <!-- END Statistic block -->
                                </div>
                            </div>
                            <div class="uk-hidden-small uk-width-medium-1-2">
                                <!-- START Promo block -->
                                <div class="uk-panel uk-hidden-small">
                                    <a class="tm-promo-video" href="https://vimeo.com/31012462" data-uk-lightbox="{group:'group2'}">
                                        <img src="<?= base_url() ?>assets/images/demo/visuals/promo-videzso.jpg" width="610" height="380" alt="Promo Video"> 
                                    </a>
                                </div>
                                <!-- END Promo block -->
                            </div>
                        </section>
                    </div>
                </div>
                <!-- END Statistic and Promo block -->
            @endif
            
            <!-- START Pricing block -->
            <div class="tm-block tm-block-bottom-b tm-section-dark tm-section-image tm-section-padding-large">
                <div class="uk-container uk-container-center">
                    <section class="tm-bottom-b uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1">
                            <!-- START Pricing intro text block -->
                            <div class="uk-panel">
                                <h3 class="uk-panel-title uk-text-center">COSTOS</h3>
                            </div>
                            <!-- END Pricing intro text block -->
                            
                            <!-- START Pricing table block -->
                            @if (Horarios::where('estado', '1')->exists())
                            @foreach (Horarios::all() as $row)
                            <?php $costo = money_format('%.2n', $row->costo) ?>
                            <div class="uk-panel">
                                <div class="uk-grid tm-price" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                    <div class="uk-width-medium-1-4 uk-width-small-1-2 uk-text-center">
                                        <div class="uk-panel uk-float-right uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-1">
                                            <div class="price">${{ explode('.', $costo)[0] }}<sup>.{{ explode('.', $costo)[1] }}</sup></div>
                                            <h2 class="uk-panel-title uk-margin-top">precio</h2>
                                            <p class="tm-panel-subtitle">{{ $row->curso()->disciplina }}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <div class="uk-panel">
                                <div class="uk-grid tm-price" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">
                                    <div class="uk-width-medium-1-4 uk-width-small-1-2 uk-text-center">
                                        <div class="uk-panel uk-float-right uk-width-small-1-1 uk-width-medium-1-1 uk-width-large-1-1">
                                            <div class="price">$00<sup>.00</sup></div>
                                            <h2 class="uk-panel-title uk-margin-top">precio</h2>
                                            <p class="tm-panel-subtitle">Disciplina</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!-- END Pricing table block -->
                        </div>
                    </section>
                </div>
            </div>
            <!-- END Pricing block -->

            <!-- START Trainers block -->
            <div class="tm-block  tm-block-bottom-a tm-section-light tm-section-padding-large">
                <div class="uk-container uk-container-center">
                    <section class="tm-bottom-a uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
                        <div class="uk-width-1-1">
                            <!-- START Trainers intro text block -->
                            <div class="uk-panel">
                                <h3 class="uk-panel-title uk-text-center">Instructores</h3>
                            </div>
                            <!-- END Trainers intro text block -->
                            
                            <!-- START Trainers foto block -->
                            <div class="uk-panel">
                                <div class="category-module-trainers ">
                                    <div class="uk-grid" data-uk-grid-margin="" data-uk-grid-match="{target:'.uk-panel'}">

                                        @if (Perfiles::where('estado', '1')->where('level_to', '3')->exists())
                                        @foreach (Perfiles::all()->where('estado', '1')->where('level_to', '3') as $row)
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 1 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    @if ($row->img_perfil)
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>aploads/{{ $row->img_perfil }}');">
                                                        <img src="<?= base_url() ?>uploads/{{ $row->img_perfil }}" class="uk-invisible" width="295" height="500" alt="Tyler Brown">                                
                                                    @else
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/trainers/trainer-8.jpg');">
                                                        <img src="<?= base_url() ?>assets/images/demo/trainers/trainer-8.jpg" class="uk-invisible" width="295" height="500" alt="Tyler Brown">                            
                                                    @endif
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="trainers/edward-j-hopper.html">{{ $row->get_simple_name() }}</a></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 1 block -->
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="uk-width-medium-1-2 uk-width-large-1-4 uk-text-left">
                                            <!-- START Trainer 1 block -->
                                            <div class="uk-width-1-1">
                                                <div class="uk-panel">
                                                    <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/trainers/trainer-8.jpg');">
                                                        <img src="<?= base_url() ?>assets/images/demo/trainers/trainer-8.jpg" class="uk-invisible" width="295" height="500" alt="Tyler Brown">                            
                                                        <div class="uk-position-cover uk-flex uk-flex-center uk-flex-bottom" style="overflow: hidden;">
                                                            <div class="ak-overlay uk-width-1-1">
                                                                <h4 class="uk-panel-title uk-margin-remove"><a href="trainers/edward-j-hopper.html">Tyler Brown</a></h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Trainer 1 block -->
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                            <!-- END Trainers foto block -->
                        </div>
                    </section>
                </div>
            </div>
            <!-- END Trainers block -->
            
            <!-- START Social block -->
            <div class="tm-block tm-block-social">
                <div class="uk-container uk-container-center">
                    <div class="uk-panel ak-social">
                        <ul class="uk-subnav uk-subnav-line uk-text-center">
                            <li><a href="https://www.facebook.com/AquaroEcole/" target="_blank">Facebook</a></li>
                            <li><a href="http://instagram.com/"  target="_blank">Instagram</a></li>
                            <li><a href="https://www.whatsapp.com"  target="_blank">Whatsapp</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END Social block -->
            
            <!-- START Footer block -->
            @include('include/footer')
            <!-- END Footer block -->
            
            <!-- START Mobile menu block -->
            <div id="offcanvas" class="uk-offcanvas">
                <div class="uk-offcanvas-bar">
                    <ul class="uk-nav uk-nav-offcanvas">
                        @include('include/nav')
                    </ul>
                </div>
            </div>
            <!-- END Mobile menu block -->            
            
        </div>

        <script src="<?= base_url() ?>assets/js/jquery.min.js" type="text/javascript"></script><!-- jQuery v1.11.2 -->
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script><!-- Bootstrap.js Custom version for HTML! -->
        
        <!-- UIkit Version 2.19.0 -->
        <script src="<?= base_url() ?>assets/js/uikit/js/uikit.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/slideshow.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/slideshow-fx.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/core/cover.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/core/modal.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/lightbox.js" type="text/javascript"></script>
        
        <!-- Animated circular progress bars -->
        <script src="<?= base_url() ?>assets/js/circle-progress.js" type="text/javascript"></script>
        
        <!-- START Schedule block -->
        <script src="<?= base_url() ?>assets/js/jquery.mousewheel.js" type="text/javascript"></script><!-- Uses for Schedule -->
        <script src="<?= base_url() ?>assets/js/jquery.jscrollpane.min.js" type="text/javascript"></script><!-- Uses for Schedule -->
        <!-- END Schedule block -->
        
        <!-- Template scripts -->
        <script src="<?= base_url() ?>assets/js/script.js" type="text/javascript"></script>
        
    </body>
</html>