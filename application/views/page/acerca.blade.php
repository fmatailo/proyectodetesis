@extends('template/base')

@section('content')
    <div class="uk-container uk-container-center">
        <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
            <!-- START sidebar-a block -->
            <aside class="tm-sidebar-a uk-width-large-1-4 uk-width-small-1-1">
                <div class="uk-panel uk-panel-header uk-panel-box">
                    <h3 class="uk-panel-title"><i class="uk-icon-tag"></i> Misión</h3>
                    <p style="text-align: justify"> Durante esta acatividad noble y sana que beneficia tanto a niñez y juventud lojanos sea la principal ocupación sana de nuestros niños, jóvenes y adultos y que se incluye como parte de la cultura lojana(Cultura Natatoria). alumnos.
                    </p>
                    <p style="text-align: justify">Si tenemos un niño(a) pequeño, los invitamos a participar de nuestras actividades acuáticas, actividad fisica válida para estimular en edad temprana, propicia para un desarrollo holístico
                    </p>
                </div>
                <div class="uk-panel uk-panel-header uk-panel-box">
                    <h3 class="uk-panel-title"><i class="uk-icon-tag"></i> Visión</h3>
                    <p style="text-align: justify">Ejecutamos el trabajo seguro que estas actividades son la únicas herramientas que garantizan una sociedad sana y honesta; por ello nos hemos propuesto, dentro de una iniciativa fomentar, motivar y orientar y asegurar la practica deportiva de la natación en la comunidad lojana.</p>
                </div>
            </aside>
            <!-- END sidebar-a block -->
            <div class="tm-main uk-width-large-3-4 uk-width-small-1-1" style="min-height: 2258px;">
                <main class="tm-content uk-position-relative">
                
                    <br>
                    
                    <div id="system-message-container"></div>
                    
                    <!-- START Dummy item block -->
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Quienes Somos
                        </h1>
                        <div>
                            <p style="text-align: justify">La academia de natación aQuaro Ecole fue creada en septiembre de 1973 se encuentra ubicada en las calles Sabiango #13-50 y Lourdes donde ofrece varios servicos tales como:</p>
                            <p style="text-align: justify">Hidroterapia, matronatación, estimulación temprana, estilos de natación, curos de natación, natación infantil y cursos vacacionales y por lo que cuenta también con su propio gimnacio y cursos de bailo terapia. que deseen aprender un ritmo especifico o ser profesionales en el contexto artístico del baile. 
                                </p>
                            <p style="text-align: justify">Cada una de la disciplina deportivas cuenta para cada actividad su propio instructor y su respectivo horario para la realización de dicha disciplina deportiva esta academia cuenta con 45 años de experiencia.</p>
                        </div>
                    </article>
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Objetivo Principal
                        </h1>
                        <div>
                            <p style="text-align: justify">Valorar el esfuerzo y la dedicación de los alumnos, reconociendo en cada uno su potencial creativo, abandonando toda posibilidad de discriminación de edades, clases sociales o culturales.</p>
                        </div>
                    </article>
                    <article class="uk-article">
                        <h1 class="uk-article-title">
                            Principios
                        </h1>
                        <div>
                            <p style="text-align: justify">Formar parte de las diciplinas natatorias a través de la unión de profesionales dispuestos a brindar un servicio con calidad en la enseñanza a nuestros alumnos con responsabilidad y compromiso para Generar ideas innovadoras que desarrollen su talento mediante la correcta organización que caracteriza a la academia de natación “aQuaro Ecole” teniendo una variada programación, acertada planeación que genere al alumno seguridad Confianza, cumplimiento y superación de expectativas.
                               </p>
                        </div>
                    </article>
                    <!-- END Dummy item block -->
                    
                </main>
            </div>
            
        </div>
    </div>
@endsection