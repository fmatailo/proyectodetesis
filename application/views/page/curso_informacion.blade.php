@extends('template/base')

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>                
                <div id="system-message-container"></div>
                
                
                <!-- START Article Blog block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">{{ $curso->disciplina }}</h1>
                    <div>
                        <div class="uk-panel tm-classes">
                            @if ($curso->img_curso)
                            <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>uploads/{{ $curso->img_curso }}');">
                                <img src="<?= base_url() ?>uploads/{{ $curso->img_curso }}" class="uk-invisible" width="610" height="640" alt="5 Unique Workauts to Improve your Deadlift">
                            </div>    
                            @else
                            <div class="uk-cover-background uk-position-relative" style="background-image: url('<?= base_url() ?>assets/images/demo/classes/class-6.jpg');">
                                <img src="<?= base_url() ?>assets/images/demo/classes/class-6.jpg" class="uk-invisible" width="610" height="640" alt="5 Unique Workauts to Improve your Deadlift">
                            </div>
                            @endif
                        </div>
                        <p>{{ $curso->descripcion }}</p>
                    </div>
                </article>
                <!-- END Article Blog block -->
                
                
            </main>
        </div>
    </div>
</div>
@endsection