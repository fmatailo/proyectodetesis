@extends('template/base')

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>                
                <div id="system-message-container"></div>
                
                <h1>Instructor</h1>
                
                <!-- START Article Blog block -->
                <article class="uk-article">
                    <h2 class="uk-article-title">
                        {{ $coreografo->get_simple_name() }}
                    </h2>
                    @if ($coreografo->img_perfil)
                    <img class="uk-align-left" src="<?= base_url() ?>uploads/{{ $coreografo->img_perfil }}" alt="">
                    @else
                    <img class="uk-align-left" src="<?= base_url() ?>assets/images/demo/trainers/trainer-1.jpg" alt="">
                    @endif
                    <div>
                        <p>{{ $coreografo->informacion }}</p>
                    </div>
                </article>
                <!-- END Article Blog block -->
            </main>
        </div>
    </div>
</div>
@endsection