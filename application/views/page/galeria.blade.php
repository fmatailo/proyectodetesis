@extends('template/base')

@section('style')
<!-- Gallery Styles -->
<link href="<?php base_url(); ?>assets/css/gallery.css" rel="stylesheet">
@endsection

@section('content')
<div class="uk-container uk-container-center">
    <div class="tm-middle uk-grid" data-uk-grid-match="" data-uk-grid-margin="">
        <div class="tm-main uk-width-medium-1-1">
            <main class="tm-content uk-position-relative">
                <br>
                <div id="system-message-container"></div>
                
                <!-- START Gallery block -->
                <article class="uk-article">
                    <h1 class="uk-article-title">
                        GALERIA
                    </h1>
                    <div>
                        <div>
                            <div id="rg-151" class="rokgallery-wrapper">
                                <div class="rg-gm-container cols3">
                                    <div class="rg-gm-slice-container">
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Armwrestling" href="<?php base_url(); ?>assets/images/gallery/gallery_1.png">
                                                            <img title="Armwrestling" alt="Armwrestling" src="<?php base_url(); ?>assets/images/gallery/gallery_1_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Cursos de natación</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="The Dumbbell Workout" href="<?php base_url(); ?>assets/images/gallery/gallery_2.jpg">
                                                             <img title="The Dumbbell Workout" alt="The Dumbbell Workout" src="<?php base_url(); ?>assets/images/gallery/gallery_2_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Corrección de estilos</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="The Dumbbell Workout 2" href="<?php base_url(); ?>assets/images/gallery/gallery_3.png">
                                                            <img title="The Dumbbell Workout 2" alt="The Dumbbell Workout 2" src="<?php base_url(); ?>assets/images/gallery/gallery_3_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Estimulación temprana</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workout Routines" href="<?php base_url(); ?>assets/images/gallery/gallery_4.png">
                                                            <img title="Workout Routines" alt="Workout Routines" src="<?php base_url(); ?>assets/images/gallery/gallery_4_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Cursos para niños</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Tennis" href="<?php base_url(); ?>assets/images/gallery/gallery_5.jpg">
                                                            <img title="Tennis" alt="Tennis" src="<?php base_url(); ?>assets/images/gallery/gallery_5_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Cursos fines de semana</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Training with rope" href="<?php base_url(); ?>assets/images/gallery/gallery_6.png">
                                                            <img title="Training with rope" alt="Training with rope" src="<?php base_url(); ?>assets/images/gallery/gallery_6_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Horario de disciplinas</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="rg-gm-slice-list">
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workouts 2" href="<?php base_url(); ?>assets/images/gallery/gallery_7.png">
                                                            <img title="Workouts 2" alt="Workouts 2" src="<?php base_url(); ?>assets/images/gallery/gallery_7_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Matronatación</span>
                                                    
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Athletics" href="<?php base_url(); ?>assets/images/gallery/gallery_8.png">
                                                            <img title="Athletics" alt="Athletics" src="<?php base_url(); ?>assets/images/gallery/gallery_8_thumb.png" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Cursos vacacionales</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rg-gm-slice-item">
                                                    <div class="rg-gm-slice">	               
                                                        <a data-uk-lightbox="{group:'gallery-group'}" title="Workouts" href="<?php base_url(); ?>assets/images/gallery/gallery_9.jpg">
                                                            <img title="Workouts" alt="Workouts" src="<?php base_url(); ?>assets/images/gallery/gallery_9_thumb.jpg" width="400" height="250">
                                                        </a>
                                                    </div>
                                                    <div class="rg-gm-info">
                                                        <span class="rg-gm-title">Aprendizaje y estílos</span>
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
                <!-- END Gallery block -->
            </main>
        </div>
    </div>
</div>
@endsection