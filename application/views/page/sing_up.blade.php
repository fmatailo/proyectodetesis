<html>
	<head>
		<title>Academia de natación</title>
		<link href="<?= base_url() ?>assets/css/estilos.css" rel="stylesheet"/>
		<link href="<?= base_url() ?>assets/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
		<meta charset="utf-8">
	</head>
	<body>
		<?php echo validation_errors(); ?>
		<?php
		$attr = array(
			'class' => 'contacto'
		)
		?>
		<?php echo form_open_multipart('', $attr); ?>
		   <div><img src="<?= base_url() ?>assets/images/usuario.png" class='imgRedonda'/></div>			
           <div>
				<label for="cedula">CÉDULA:</label>
				<input type='number' id="cedula" min='0' value='<?= set_value('cedula') ?>' name="cedula" required>
			</div>
           	<div>
				<label for="apellidos">APELLIDOS:</label>
				<input type='text' id="apellidos" value='<?= set_value('apellidos') ?>' name="apellidos" required>
			</div>
           	<div>
				<label for="nombres">NOMBRES:</label>
				<input type='text' id="nombres" value='<?= set_value('nombres') ?>' name="nombres" required>
			</div>
           	<div>
				<label for="doreccion">DIRECCIÓN:</label>
				<textarea type='text' id="direccion" rows=3 name="direccion"><?= set_value('direccion') ?></textarea>
			</div>
		   	<div>
				<label for="email">EMAIL:</label>
				<input type='email' id="email" value='<?= set_value('email') ?>' name="email" required>
			</div>
			<label for="genero">GENERO:</label><br>
           <input type="radio" name="genero" value="0"> FEMENINO
           <input type="radio" name="genero" value="1"> MASCULINO
		   </p>
		   <br>
		   <div>
				<label for="telefono">TELÉFONO:</label>
				<input type='number' min="0" id="telefono" value='<?= set_value('telefono') ?>' name="telefono" required>
			</div>
		   	<div>
				<label for="edad">EDAD:</label>
				<input type='number' min="0" id="edad" value='<?= set_value('edad') ?>' name="edad" required>
			</div>
		   	<div>
				<label>IMG_PERFIL:</label><br>
				<input type="file" id="" name="img_perfil">
			</div>
           	<div>
				<label for="username">USUARIO:</label>
				<input type='text' id="username" value='<?= set_value('username') ?>' name="username" required>
			</div>
           	<div>
				<label for="password">CONTRASEÑA:</label>
				<input type="password" id="password" value='' name="password" required>
			</div>
			<div>
				<label for="password2">REPITA LA CONTRASEÑA:</label>
				<input type="password" id="password2" value='' name="password2" required>
			</div>
			<br>

           <input type="submit" class="btn btn-success" value="Aceptar">
           <a href="/inicio" class="boton">Cancelar</a>
           
		</form>
	</body>
</html>