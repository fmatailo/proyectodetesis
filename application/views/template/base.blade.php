<!DOCTYPE HTML>
<html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="With unique design and accent in details Maxx Fitness is perfect template. Design have beautiful typography and elegant structure. Joomla Template is based on Warp7 Framework and made for all who wants a lightweight and modular website.">
        
        <title>Academia de natacion</title>
        
        <!-- Favicon and Apple icon -->
        <link href="<?= base_url() ?>assets/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
        <link href="apple_touch_icon.png" rel="apple-touch-icon-precomposed">
        
        <link href="<?= base_url() ?>assets/css/bootstrap.css" rel="stylesheet"><!-- Bootstrap Styles -->
        <link href="<?= base_url() ?>assets/css/theme.css" rel="stylesheet"><!-- Main Template Styles -->
        <link href="<?= base_url() ?>assets/css/schedule.css" rel="stylesheet"><!-- Schedule CSS -->

        @yield('style')
        
    </head>
    <body class="tm-isblog">
        <div class="ak-page">
            <!-- START Main menu -->
            <nav class="tm-navbar uk-navbar uk-navbar-attached">
                <div class="uk-container uk-container-center">
                    <!-- START Logo -->
                    <a class="tm-logo" href="/">
                        <span class="color-1">aQuaro</span><span class="color-2">Ecole</span>
                    </a>
                    <!-- END Logo -->
                    <ul class="uk-navbar-nav uk-hidden-small">
                        @include('include/nav')
                    </ul>
                    <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas=""></a>
                </div>
            </nav>
            <!-- END Main menu -->
            
            <!-- START Central block -->
            @yield('content')
            <!-- END Central block -->
            
            
            <!-- START Social block -->
            <div class="tm-block tm-block-social">
                <div class="uk-container uk-container-center">
                    <div class="uk-panel ak-social">
                        <ul class="uk-subnav uk-subnav-line uk-text-center">
                            <li><a href="https://www.facebook.com/AquaroEcole/" target="_blank">Facebook</a></li>
                            <li><a href="http://instagram.com/"  target="_blank">Instagram</a></li>
                            <li><a href="https://www.whatsapp.com"  target="_blank">Whatsapp</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- END Social block -->
            
            <!-- START Footer block -->
            @include('include/footer')
            <!-- END Footer block -->
            
            <!-- START Mobile menu block -->
            <div id="offcanvas" class="uk-offcanvas">
                <div class="uk-offcanvas-bar">
                    <ul class="uk-nav uk-nav-offcanvas">
                        @include('include/nav')
                    </ul>
                </div>
            </div>
            <!-- END Mobile menu block -->            
            
        </div>

        <script src="<?= base_url() ?>assets/js/jquery.min.js" type="text/javascript"></script><!-- jQuery v1.11.2 -->
        <script src="<?= base_url() ?>assets/js/bootstrap.min.js" type="text/javascript"></script><!-- Bootstrap.js Custom version for HTML! -->
        
        <!-- UIkit Version 2.19.0 -->
        <script src="<?= base_url() ?>assets/js/uikit/js/uikit.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/slideshow.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/slideshow-fx.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/core/cover.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/core/modal.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/js/uikit/js/components/lightbox.js" type="text/javascript"></script>
        
        <!-- Animated circular progress bars -->
        <script src="<?= base_url() ?>assets/js/circle-progress.js" type="text/javascript"></script>
        
        <!-- START Schedule block -->
        <script src="<?= base_url() ?>assets/js/jquery.mousewheel.js" type="text/javascript"></script><!-- Uses for Schedule -->
        <script src="<?= base_url() ?>assets/js/jquery.jscrollpane.min.js" type="text/javascript"></script><!-- Uses for Schedule -->
        <!-- END Schedule block -->
        
        <!-- Template scripts -->
        <script src="<?= base_url() ?>assets/js/script.js" type="text/javascript"></script>

        @yield('script')
        
    </body>
</html>