<?php $estado = 1 ?>
@foreach ($matricula as $value)
    <table id="datatable" class="table table-striped table-bordered text-center">
        <thead>
            <tr>
                <th>#</th>
                <th>Fecha a Pagar</th>
                <th>Cuota</th>
                <th>Saldo</th>
                <th>Estado</th>
                <th>Fecha</th>
                <th width="10%">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i=0;
                $saldo=0;
            @endphp
            <?php setlocale(LC_ALL,"es_EC.utf8") ?>
            @foreach ($value->pensiones() as $row)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ strftime("%A %e %B %Y", strtotime($row->fecha_pago)) }}</td>
                <td>{{ money_format('%.2n', $row->monto) }}</td>
                <?php $saldo+=$row->monto ?>
                <td>{{ money_format('%.2n', $saldo) }}</td>
                @if ($row->estado)
                <td class="danger">
                    Pendiente
                </td>
                @else
                <td class="success">
                    Pagado
                </td>
                @endif
                <td>
                    @if ($row->fecha_pagado != '0000-00-00')
                        {{ strftime("%A %e %B %Y", strtotime($row->fecha_pagado)) }}
                    @endif
                </td>
                <td>
                    @if ($row->estado && $estado == 1)
                    <?php $estado = 0 ?>
                    <button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#exampleModal">Pagar</button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Advertencia</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                            Esta seguro que desea efectuar el pago?
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <a href="/admin/pension/pago/{{ $row->slug }}" class="btn btn-primary">Aceptar</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <script>
            $('.fade').modal('hide');
    </script>
@endforeach

