<div class="ak-footer">
    <div class="uk-container uk-container-center">
        <footer class="tm-footer uk-grid tm-footer uk-grid" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin="">
            <!-- START Footer About MaxxFitness block -->
            <div class="uk-hidden-small uk-hidden-medium uk-width-large-1-2">
                <div class="uk-panel uk-hidden-medium uk-hidden-small" style="min-height: 316px;">
                    <h3 class="uk-panel-title uk-text-center">Academia de natación</h3>
                    <p class="uk-text-justify">Formar gente deportiva a través de la Unión de profesionales dispuestos a brindar un servicio con calidad en la Enseñanza a nuestros alumnos con responsabilidad y compromiso para Generar ideas innovadoras que desarrollen su talento.</p>
                    <p> Dir: Sabiando #13-50<br> y Lourdes<br> Informe: (02)575740 / 0993858170 </p>
                    <div class="ak-copyright">©Academia de natación 2018 - 2019</div>
                    <div class="">© Autor: Franklin Valverde</div>
                </div>
            </div>
            <!-- END Footer About MaxxFitness block -->
            
            <!-- START Opening Hours block -->
            <div class="uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-2">
                <div class="uk-panel" style="min-height: 316px;">
                    <h3 class="uk-panel-title uk-text-center">Horarios de Atención</h3>
                    <div class="uk-grid">
                        <div class="uk-width-1-2 uk-text-center">
                            <p style="line-height: 2;">
                                Lunes:<br>
                                Martes:<br>
                                Miercoles:<br>
                                Jueves:<br>
                                Viernes: <br>
                                Sabadó:    
                            </p>
                        </div>
                        <div class="uk-width-1-2 uk-text-center">
                            <p style="line-height: 2;">
                                8:30AM : 17:30PM<br>
                                8:30AM : 17:30PM<br>
                                8:30AM : 17:30PM<br>
                                8:30AM : 17:30PM<br>
                                8:30AM : 17:30PM<br>
                               10:30AM : 12:30PM
                            </p>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div id="aiContactSafe_form_1" class="uk-text-center">
                        <a href="/page/sing_up"" id="aiContactSafeSendButton" style="margin:35%;"><strong>INSCRÍBETE</strong></a>
                    </div>
                </div>
            </div>
            <!-- END Opening Hours block -->
            
        </footer>
    </div>
</div>