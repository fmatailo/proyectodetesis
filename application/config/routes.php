<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'page/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# Migrations
$route['migrate/(:num)'] = 'migrate/index/$1';
# Simple Pages
$route['(:any)'] = 'page/index/$1';
# Simple course information Pages
$route['curso/(:any)'] = 'page/course/$1';
# Simple coreografo information Pages
$route['coreografo/(:any)'] = 'page/coreografo/$1';

# Index Admin Pages
$route['admin/inicio'] = 'admin/page/inicio';

# Index Admin Contenido
$route['admin/contenido'] = 'admin/page/contenido';

# Logout Page
$route['admin/logout'] = 'admin/page/logout';

# Logout Page
$route['admin/respaldo'] = 'admin/page/respaldo';

# Api Perfil Coreografo Routes
$route['admin/coreografo/crear'] = 'admin/usuario/post/coreografo';
$route['admin/coreografo/tabla'] = 'admin/usuario/get/coreografo';
$route['admin/coreografo/modificar/(:any)'] = 'admin/usuario/put/coreografo/$1';
$route['admin/coreografo/eliminar/(:any)'] = 'admin/usuario/delete/coreografo/$1';
$route['admin/coreografo/reporte'] = 'admin/usuario/report/coreografo';

# Api Perfil Usuario Routes
$route['admin/usuario/crear'] = 'admin/usuario/post/usuario';
$route['admin/usuario/tabla'] = 'admin/usuario/get/usuario';
$route['admin/usuario/modificar/(:any)'] = 'admin/usuario/put/usuario/$1';
$route['admin/usuario/eliminar/(:any)'] = 'admin/usuario/delete/usuario/$1';

# Api Perfil Cliente Routes
$route['admin/cliente/tabla'] = 'admin/usuario/get/cliente';
$route['admin/cliente/eliminar/(:any)'] = 'admin/usuario/delete/cliente/$1';
$route['admin/cliente/reporte'] = 'admin/usuario/report/cliente';

# Api Inscripcion Routes
$route['admin/inscripcion/tabla'] = 'admin/inscripcion/get';
$route['admin/inscripcion/reporte'] = 'admin/inscripcion/report';

#Api Curso Routes
$route['admin/curso/crear'] = 'admin/curso/post';
$route['admin/curso/tabla'] = 'admin/curso/get';
$route['admin/curso/reporte'] = 'admin/curso/report';

#Api Horario Routes
$route['admin/horario/crear'] = 'admin/horario/post';
$route['admin/horario/tabla'] = 'admin/horario/get';
$route['admin/horario/reporte'] = 'admin/horario/report';

#Api Matricula Routes
$route['admin/matricula/crear/(:any)'] = 'admin/matricula/post/$1';
$route['admin/matricula/tabla'] = 'admin/matricula/get';
$route['admin/matricula/reporte'] = 'admin/matricula/report';

#Api Pension Routes
$route['admin/pension/pagar'] = 'admin/pension/post';
$route['admin/pension/pago/(:any)'] = 'admin/pension/put/$1';
$route['admin/pension/tabla'] = 'admin/pension/get';
$route['admin/pension/reporte'] = 'admin/pension/report';