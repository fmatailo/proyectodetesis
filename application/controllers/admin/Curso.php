<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Curso extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * @method: CREATE DATA perfil IN DATABASE
	 * @param null
	 */
    public function post(){
        $level = array(1, 2);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => strtoupper('CREAR CURSO')
            );
            if (isset($_FILES['img_curso'])){
                if (empty($_FILES['img_curso'])){
                    $GLOBALS['image_size'] = 90000;
                    $GLOBALS['image_width'] = 1024;
                    $GLOBALS['image_height'] = 768;
                    $GLOBALS['image'] = $_FILES['img_curso'];
                }
            }
            # Validation
            $this->form_validation->set_rules('disciplina', 'Nombre de disciplina', 'trim|required|max_length[99]|is_unique[curso.disciplina]');
            $this->form_validation->set_rules('descripcion', 'trim|Descripcion', 'required');
            if (isset($_FILES['img_curso'])){
                if (empty($_FILES['img_curso'])){
                    $this->form_validation->set_rules('img_curso', 'Imagen', array('trim', array(
                        'option', function($opt){
                            $image = getimagesize($GLOBALS['image']['tmp_name']);
                            if ($image[0] > $GLOBALS['image_width']){
                                $this->form_validation->set_message('option', 'Ancho de {field} excedido');
                                return FALSE;
                            }else if ($image[1] > $GLOBALS['image_height']){
                                $this->form_validation->set_message('option', 'Alto de {field} excedido');
                                return FALSE;
                            }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                                $this->form_validation->set_message('option', 'Peso de {field} excedido');
                                return FALSE;
                            }
                            return TRUE;
                        }
                    )));
                }
            }
            # /Validation
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/curso", $data);
            }else{
                $img = '';
                $file = 'curso/';
                if ($_FILES['img_curso']['name']){
                    $config = array(
                        'upload_path'   => FCPATH.'/uploads/'.$file,
                        'allowed_types' => "jpg|png|jpeg",
                        'overwrite'     => false,
                        'max_size'      => $GLOBALS['image_size'],
                        'max_width'     => $GLOBALS['image_width'],
                        'max_height'    => $GLOBALS['image_height']
                    );
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('img_curso')){
                        $img = "{$file}{$this->upload->data('file_name')}";
                    }
                }
                $data = array(
                    'disciplina' => $this->input->post('disciplina'),
                    'descripcion' => $this->input->post('descripcion'),
                    'img_curso' => $img,
                    'estado'  => '1'
                );
                $this->load->model('modelos');
                $this->modelos->curso($data);
                redirect('admin/inicio');
            }
        }else {
            redirect('admin/inicio');
        }
    }

    public function get(){
        $level = array(1, 2);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'TABLA CURSO',
            );
            echo $this->blade->view()->make("admin/tabla/curso", $data);
        }else{
            redirect('admin/inicio');  
        }
    }

    public function put(){
		
    }

    public function delete(){
		
    }
    
    public function report(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $this->load->library('pdf');
            $this->pdf = new Pdf();
            $this->pdf->SetTitleHead('REPORTE CURSOS');
            $this->pdf->AddPage();
            $this->pdf->AliasNbPages();
            $info = array();
            $i=0;
            foreach (Cursos::all()->where('estado', '1') as $row){
                array_push($info, array(++$i, $row->disciplina, ($row->estado) ? 'Activo' : 'Inactivo'));
            }
            $data = array(
                'cabecera'=>array('Nro', 'Disciplina', 'Estado'),
                'contenido'=>$info,
                'tamaño'=>array(15, 135, 35)
            );
            $this->pdf->head_table($data['cabecera'], $data['tamaño']);
            $this->pdf->SetWidths($data['tamaño']);
            $this->pdf->table($data['contenido']);
            $this->pdf->Output();
        }else{
            redirect('admin/inicio');
        }
    }
}