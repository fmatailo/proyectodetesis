<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Usuario extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
     * @method: Para crearción de usuarios en la base de datos
     */
    public function post($user){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => strtoupper('CREAR '.$user)
            );
            $role = array('usuario', 'coreografo');
            if (!in_array($user, $role)){
                show_404();
            }
            if (isset($_FILES['img_perfil'])){
                if (empty($_FILES['img_perfil'])){
                    $GLOBALS['image_size'] = 90000;
                    $GLOBALS['image_width'] = 1024;
                    $GLOBALS['image_height'] = 768;
                    $GLOBALS['image'] = $_FILES['img_perfil'];
                }
            }
            # Validation
            $this->form_validation->set_rules('cedula', 'Cedula', array('trim', 'required', 'integer', 'max_length[10]', 'min_length[10]', 'is_unique[perfil.cedula]', array(
                'option', function($opt){
                    $ult_digito=substr($opt, -1,1);//extraigo el ultimo digito de la cedula
                    //extraigo los valores pares//
                    $valor2=substr($opt, 1, 1);
                    $valor4=substr($opt, 3, 1);
                    $valor6=substr($opt, 5, 1);
                    $valor8=substr($opt, 7, 1);
                    $suma_pares=($valor2 + $valor4 + $valor6 + $valor8);
                    //extraigo los valores impares//
                    $valor1=substr($opt, 0, 1);
                    $valor1=($valor1 * 2);
                    if($valor1>9){ 
                        $valor1=($valor1 - 9); 
                    }
                    $valor3=substr($opt, 2, 1);
                    $valor3=($valor3 * 2);
                    if($valor3>9){ 
                        $valor3=($valor3 - 9); 
                    }
                    $valor5=substr($opt, 4, 1);
                    $valor5=($valor5 * 2);
                    if($valor5>9){ 
                        $valor5=($valor5 - 9); 
                    }
                    $valor7=substr($opt, 6, 1);
                    $valor7=($valor7 * 2);
                    if($valor7>9){ 
                        $valor7=($valor7 - 9); 
                    }
                    $valor9=substr($opt, 8, 1);
                    $valor9=($valor9 * 2);
                    if($valor9>9){ 
                        $valor9=($valor9 - 9); 
                    }
                    $suma_impares=($valor1 + $valor3 + $valor5 + $valor7 + $valor9);
                    $suma=($suma_pares + $suma_impares);
                    $dis=substr($suma, 0,1);//extraigo el primer numero de la suma
                    $dis=(($dis + 1)* 10);//luego ese numero lo multiplico x 10, consiguiendo asi la decena inmediata superior
                    $digito=($dis - $suma);
                    if($digito==10){ 
                        $digito='0'; 
                    }
                    if ($digito==$ult_digito){//comparo los digitos final y ultimo
                        return TRUE;
                    }else{
                        $this->form_validation->set_message('option', 'valor de {field} incorrecto');
                        return FALSE;
                    }
                }
            )));
            $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|max_length[180]');
            $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required|max_length[180]');
            $this->form_validation->set_rules('direccion', 'Direccion', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[200]|is_unique[perfil.email]');
            $this->form_validation->set_rules('genero', 'Genero', 'trim|required');
            $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required|integer|max_length[13]');
            $this->form_validation->set_rules('edad', 'Edad', 'trim|required|integer');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[perfil.username]');
            $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|max_length[20]');
            $this->form_validation->set_rules('password2', 'Confirmacion Contraseña', 'trim|required|matches[password]');
            if (isset($_FILES['img'])){
                if (empty($_FILES['img'])){
                    $this->form_validation->set_rules('img_perfil', 'Imagen', array('trim', array(
                        'option', function($opt){
                            $image = getimagesize($GLOBALS['image']['tmp_name']);
                            if ($image[0] > $GLOBALS['image_width']){
                                $this->form_validation->set_message('option', 'Ancho de {field} excedido');
                                return FALSE;
                            }else if ($image[1] > $GLOBALS['image_height']){
                                $this->form_validation->set_message('option', 'Alto de {field} excedido');
                                return FALSE;
                            }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                                $this->form_validation->set_message('option', 'Peso de {field} excedido');
                                return FALSE;
                            }
                            return TRUE;
                        }
                    )));
                }
            }
            # /Validation
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/".$user, $data);
            }else{
                $level = null;
                $img = '';
                $file = 'avatar/';
                if ($_FILES['img_perfil']['name']){
                    $config = array(
                        'upload_path'   => FCPATH.'/uploads/'.$file,
                        'allowed_types' => "jpg|png|jpeg",
                        'overwrite'     => false,
                        'max_size'      => $GLOBALS['image_size'],
                        'max_width'     => $GLOBALS['image_width'],
                        'max_height'    => $GLOBALS['image_height']
                    );
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('img_perfil')){
                        $img = "{$file}{$this->upload->data('file_name')}";
                    }
                }
                $informacion = '';
                if (isset($_POST['informacion'])){
                    $informacion = $this->input->post('informacion');
                }
                $data = array(
                    'cedula' => $this->input->post('cedula', TRUE),
                    'apellidos' => $this->input->post('apellidos', TRUE),
                    'nombres' => $this->input->post('nombres', TRUE),
                    'direccion' => $this->input->post('direccion', TRUE),
                    'email' => $this->input->post('email', TRUE),
                    'genero' => $this->input->post('genero', TRUE),
                    'telefono' => $this->input->post('telefono', TRUE),
                    'edad' => $this->input->post('edad', TRUE),
                    'estado' => '1',
                    'username' => $this->input->post('username', TRUE),
                    'password' => $this->input->post('password', TRUE),
                    'img_perfil' => $img,
                    'informacion' => $informacion,
                    'tipo' => $user
                );
                $this->load->model('modelos');
                $this->modelos->perfil($data);
                redirect('admin/inicio');
            }
        }else{
            redirect('admin/inicio');
        }
    }

    /**
     * @method: Para obterner todos los resultados de base de datos y ponerlos en una tabla
     */
    public function get($page){
        $level = array(1,3);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => strtoupper('TABLA '.$page),
            );
            $role = array('usuario', 'coreografo');
            echo $this->blade->view()->make("admin/tabla/".$page, $data);
        }else{
            redirect('admin/inicio');
        }
    }

    /**
     * @method:Para modifcar datos del usuario  
     */
    public function put($user, $slug){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $instance = Perfiles::where('slug', $slug)->first();
            $data = array(
                'title' => strtoupper('MODIFICAR '.$user),
                'instance' => $instance
            );
            $role = array('usuario', 'coreografo');
            if (!in_array($user, $role)){
                show_404();
            }
            if (isset($_FILES['img_perfil'])){
                if (empty($_FILES['img_perfil'])){
                    $GLOBALS['image_size'] = 90000;
                    $GLOBALS['image_width'] = 1024;
                    $GLOBALS['image_height'] = 768;
                    $GLOBALS['image'] = $_FILES['img_perfil'];
                }
            }
            # Validation
            $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim|required|max_length[180]');
            $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required|max_length[180]');
            $this->form_validation->set_rules('direccion', 'Direccion', 'trim|required');
            $this->form_validation->set_rules('telefono', 'Telefono', 'trim|required|integer|max_length[13]');
            if (isset($_FILES['img_perfil'])){
                if (empty($_FILES['img_perfil'])){
                    $this->form_validation->set_rules('img_perfil', 'Imagen', array('trim', array(
                        'option', function($opt){
                            $image = getimagesize($GLOBALS['image']['tmp_name']);
                            if ($image[0] > $GLOBALS['image_width']){
                                $this->form_validation->set_message('option', 'Ancho de {field} excedido');
                                return FALSE;
                            }else if ($image[1] > $GLOBALS['image_height']){
                                $this->form_validation->set_message('option', 'Alto de {field} excedido');
                                return FALSE;
                            }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                                $this->form_validation->set_message('option', 'Peso de {field} excedido');
                                return FALSE;
                            }
                            return TRUE;
                        }
                    )));
                }
            }
            if ($this->input->post('password')){
                $this->form_validation->set_rules('password', 'Contraseña', 'trim|required|max_length[20]');
                $this->form_validation->set_rules('password2', 'Confirmacion Contraseña', 'trim|required|matches[password]');
            }
            # /Validation
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/".$user."_modificar", $data);
            }else{
                $img = '';
                $file = 'avatar/';
                if (isset($_FILES['img_perfil']['name'])){
                    $config = array(
                        'upload_path'   => FCPATH.'/uploads/'.$file,
                        'allowed_types' => "jpg|png|jpeg",
                        'overwrite'     => false,
                        'max_size'      => $GLOBALS['image_size'],
                        'max_width'     => $GLOBALS['image_width'],
                        'max_height'    => $GLOBALS['image_height']
                    );
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload('img_perfil')){
                        $img = "{$file}{$this->upload->data('file_name')}";
                    }
                }
                # Guardar
                $instance->nombres = $this->input->post('nombres', TRUE);
                $instance->apellidos = $this->input->post('apellidos', TRUE);
                $instance->direccion = $this->input->post('direccion', TRUE);
                $instance->telefono = $this->input->post('telefono', TRUE);
                if ($img){
                    $instance->img_perfil = $img;
                }
                if ($this->input->post('password', TRUE)){
                    $instance->password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                }
                if ($this->input->post('informacion', TRUE)){
                    $instance->informacion = $this->input->post('informacion');
                }
                $instance->save();
                # Guardar
                redirect('admin/inicio');
            }
        }else{
            redirect('admin/inicio');
        }
    }
    
   /**
     * @method: Para dar de baja a los usuarios no activos 
     */ 
    public function delete($user, $slug){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $role = array('usuario', 'coreografo', 'cliente');
            if (!in_array($user, $role)){
                show_404();
            }
            $perfil = Perfiles::where('slug', $slug)->first();
            $perfil->estado = ($perfil->estado) ? '0' : '1' ;
            $perfil->save();
            redirect('/admin/'.$user.'/tabla');
        }else{
            redirect('admin/inicio');
        }
    }
    
    /**
     * @method:El reporte de usuario que se creo en la base en la aplicacion 
     */
    public function report($user){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $this->load->library('pdf');
            $this->pdf = new Pdf();
            $this->pdf->SetTitleHead('REPORTE CLIENTE Y INSTRUCTORES');
            $this->pdf->AddPage();
            $this->pdf->AliasNbPages();
            $info = array();
            $i=0;
            foreach (Perfiles::all()->where('tipo', $user) as $row){
                array_push($info, array(++$i, $row->cedula, $row->apellidos, $row->nombres, ($row->estado) ? 'Activo' : 'Inactivo' , $row->username));
            }
            $data = array(
                'cabecera'=>array('Nro', 'Cédula', 'Apellido', 'Nombre', 'Estado', 'Usuario'),
                'contenido'=>$info,
                'tamaño'=>array(20, 24, 45, 43, 20, 35)
            );
            $this->pdf->head_table($data['cabecera'], $data['tamaño']);
            $this->pdf->SetWidths($data['tamaño']);
            $this->pdf->table($data['contenido']);
            $this->pdf->Output();
        }else{
            redirect('admin/inicio');
        }
    }
}