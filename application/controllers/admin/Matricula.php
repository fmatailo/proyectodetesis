<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Matricula extends MY_Controller {

    public function __construct(){

        parent::__construct();
        if (!$this->session->username){
            redirect('/inicio');
        }

    }

    /**
	 * CREATE DATA perfil IN DATABASE
	 * @param user: role of user
	 */
    public function post($slug){
        $level = array(1, 2);
        if(in_array($this->session->level_to, $level)){
            $inscripcion = Inscripciones::where('slug', $slug)->where('estado','1')->first();
            $data = array(
                'title' => 'MATRICULA',
                'cedula' => $inscripcion->cliente()->cedula,
                'cliente' => $inscripcion->cliente()->nombres.' '.$inscripcion->cliente()->apellidos,
                'curso' => $inscripcion->horario()->curso()->disciplina,
                'horario' => $inscripcion->horario()->hora_inicio.' '.$inscripcion->horario()->hora_fin,
                'costo' => $inscripcion->horario()->costo,
                'fecha_matricula' => date('Y-m-d'),
                'slug' => $inscripcion->horario()->slug
            );
            # Validation
            $this->form_validation->set_rules('pago', 'Pago', array('trim', 'required', array(
                'option', function($opt){
                    if ($opt==1 | $opt==2){
                        return TRUE;
                    }else{
                        $this->form_validation->set_message('option', 'Ingrese opción {field} valido');
                        return FALSE;
                    }
                })
            ));
            # Validation
            if (!$this->form_validation->run()){
                echo $this->blade->view()->make("admin/formulario/matricula", $data);
            }else{
                $data = array(
                    'fecha_matricula' => date('Y-m-d'),
                    'estado' => '1',
                    'inscripcion_id' => $inscripcion->id
                );
                $this->load->model('modelos');
                $matricula = $this->modelos->matricula($data);
                $fecha = date('Y-m-d');
                if ($this->input->post('pago', TRUE) == '2') {
                    for ($i=0; $i < $inscripcion->horario()->duracion; $i++) { 
                        $data = array(
                            'fecha_pago' => $fecha,
                            'monto' => money_format('%.2n', $inscripcion->horario()->costo/$inscripcion->horario()->duracion),
                            'estado' => '1',
                            'matricula_id' => $matricula->id
                        );
                        $fecha = date("Y-m-d", strtotime(date('Y-m-d', strtotime($fecha))." + 1 months"));
                        $this->modelos->pension($data);
                    }
                }else{
                    $data = array(
                        'fecha_pago' => $fecha,
                        'monto' => money_format('%.2n', $inscripcion->horario()->costo),
                        'estado' => '1',
                        'matricula_id' => $matricula->id
                    );
                    $this->modelos->pension($data);
                }
                $inscripcion->estado = '0';
                $inscripcion->save();
                redirect('admin/inicio');
            }
        }else{
            redirect('admin/inicio');
        }
    }

    public function get(){
        $level = array(1, 2, 4);
        if(in_array($this->session->level_to, $level)){
            $data = array(
                'title' => 'TABLA MATRICULA',
            );
            echo $this->blade->view()->make("admin/tabla/matricula", $data);
        }else{
            redirect('admin/inicio');
        }
    }

    public function put(){
		
    }

    public function delete(){
		
    }
    
    public function report(){
        $level = array(1);
        if(in_array($this->session->level_to, $level)){
            $this->load->library('pdf');
            $this->pdf = new Pdf();
            $this->pdf->AddPage();
            $this->pdf->AliasNbPages();
            $info = array();
            $i=0;
            foreach (Matriculas::all()->where('estado', '1') as $row){
                array_push($info, array(++$i, $row->inscripcion()->cliente()->get_simple_name(), $row->inscripcion()->horario()->curso()->disciplina, $row->inscripcion()->horario()->hora_inicio.' '.$row->inscripcion()->horario()->hora_fin, $row->inscripcion()->horario()->fecha_inicio, $row->inscripcion()->horario()->fecha_fin));
            }
            $data = array(
                'cabecera'=>array('Nro', 'Cliente', 'Curso', 'Horario', 'Inicio', 'Finalizo'),
                'contenido'=>$info,
                'tamaño'=>array(15, 40, 35, 33, 30, 35)
            );
            $this->pdf->head_table($data['cabecera'], $data['tamaño']);
            $this->pdf->SetWidths($data['tamaño']);
            $this->pdf->table($data['contenido']);
            $this->pdf->Output();
        }else{
            redirect('admin/inicio');
        }
    }
}