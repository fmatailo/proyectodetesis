<?php

    defined('BASEPATH') OR exit('No direct script access allowed');

    class Migrate extends CI_Controller {

        public function __construct(){

            parent::__construct();
            $this->session->sess_destroy();

        }
        /**
        * @method: Para creacion de la base de datos en la aplicacion
        */

        public function index($version = 0){

            # ACTIVA (true) O INACTIVA(false) LAS MIGRACIONES
            $migrate = TRUE;

            if ($migrate){
                $this->load->library('migration');
                if ($version==='0'){

                    if ($this->migration->version(0) === FALSE){
        
                        show_error($this->migration->error_string());
        
                    }

                }else if($version==='1'){

                    ### migration->(version|current|latest)
                    if ($this->migration->latest() === FALSE){
        
                        show_error($this->migration->error_string());
        
                    }

                }else{
                    show_404();
                }
            } else {
                show_404();
            }
        }

    }